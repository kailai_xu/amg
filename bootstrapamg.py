from numpy import *
from numpy.linalg import *
from matplotlib.pyplot import *
from scipy import sparse as sps
from scipy.sparse.linalg import spsolve
from scipy.linalg import block_diag

class Config():
	def __init__(self, A, b=None):
		self.eps = 0.5
		self.A = A
		self.n = A.shape[0]

		if b is None: b = random.random(self.n)
		self.b = b
		
		self.C = None
		self.F = None
		self.xk = random.random(self.n)






def _compute_l(ll, St, U, F):
	for i in range(config.n):
		ll[i] = len(St & U) + 2*len(St & F)
		if i not in U:
			ll[i] = -1

def sparse_from_triplets(triplets, size_n, size_m):
	data = [x[2] for x in triplets]
	ii = [x[0] for x in triplets]
	jj = [x[1] for x in triplets]
	return sps.csr_matrix((data,(ii,jj)),(size_n,size_m))


def find_coarse_nodes():
	S = [None]*config.n
	St = [None]*config.n
	for i in range(config.n):
		m = 0
		for k in range(config.n):
			if config.A[i,k] < 0 and abs(config.A[i,k])>m:
				m = abs(config.A[i,k])
		S[i] = []
		if m==0: continue
		for j in range(config.n):
			if -config.A[i,j] >= config.eps*m:
				S.append(j)
	for i in range(config.n):
		S[i] = set([])
		for j in range(config.n):
			if j in S[i]: St[i].add(j)

	C = set([])
	F = set([])
	U = set(range(config.n))
	ll = [0]*config.n

	while len(U)>0:
		_compute_l(ll, St, U, F)
		i = ll.index(max(ll))
		C.add(i)
		for j in St[i]:
			if j in U:
				F.add(j)
				U.remove(j)
		U.remove(i)

	config.C = C
	config.F = F

def construct_P():
	triplets = []
	for i in config.F:
		Ni = config.A.getrow(i).sum()
		aii = config.A[i,i]
		Pi = 0.0
		for j in config.C:
			Pi += config.A[i,j]

		for j in config.C:
			aij = config.A[i,j]
			triplets.append([i,j,-Ni/Pi*aij/aii])

	for i in config.C:
		triplets.append([i,i,1.0])

	config.P = sparse_from_triplets(triplets, config.n, len(config.C))
	config.Pt = config.P.transpose(copy=True)
	config.Ac = config.Pt.dot(config.A.dot(config.P))

def relax_fine():
	# Gauss-Seidel iteration 
	r = config.b - config.A.dot(config.xk)
	config.xk = config.xk + config.S*r
	

def relax_coarse():
	r = config.b - dot(config.A, config.xk)
	rc = config.Pt.dot(r)
	# exact two layer
	Ec = spsolve(config.Ac, rc)
	config.xk = config.xk + config.P.dot(Ec)

def solver():
	for i in range(100):
		relax_fine()
		print('Residual at iter %i:%f'%(i, norm(config.b - config.A.dot(config.xk))))
		# relax_coarse()
		# print('Residual at iter %i:%f'%(i, abs(config.b - config.A.dot(config.xk))))


def genmat():
	B = array([[-4, 1, 0], [ 1, -4, 1], [ 0, 1, -4]],dtype=float64)
	A = block_diag(B, B, B)
	# Upper diagonal array offset by 3
	Dupper = diag(ones(3 * 2), 3)
	# Lower diagonal array offset by -3
	Dlower = diag(ones(3 * 2), -3)
	A += Dupper + Dlower
	return sps.csr_matrix(A)


A = genmat()
print(type(A))
# print(A)
config = Config(A)
solver()





			


			
			


