from numpy import *
from numpy.linalg import *
from matplotlib.pyplot import *
from scipy.linalg import block_diag
import sys


class AMG():
	def __init__(self, A,b,xk=None, sol=None):
		self.eps = 0.5
		self.A = A
		self.n = A.shape[0]

		self.b = b

		self.C = None
		self.F = None
		if xk is None: 
			self.xk = ones(self.n)
		else:
			self.xk = xk
		self.P = None
		self.Ac = None
		self.sol = sol

		self.find_coarse_nodes()
		self.construct_P()
		self.construct_Ac()
		# self.compute_params()

	def compute_params(self):
		S1 = (eye(self.n)-dot(inv(tril(self.A)),self.A))
		S2 = eye(self.n) - dot(self.P, dot(inv(self.Ac), dot(self.P.T, self.A)))
		print("Convergence rate of fine grid:%f"%\
			(abs(eigvals(S1)).max()))
		print("Convergence rate of find & coarse grid:%f"%\
			abs(eigvals(dot(S1,S2))).max())
		print("Matrix condition number: %f"%cond(self.A))


	def _compute_l(self,ll, St, U, F):
		for i in range(self.n):
			ll[i] = len(St[i] & U) + 2*len(St[i] & F)
			if i not in U:
				ll[i] = -1

	def find_coarse_nodes(self):
		S = [None] * self.n
		St = [None] * self.n
		for i in range(self.n):
			S[i] = set([])
			m = 0 # max
			for k in range(self.n):
				if self.A[i,k]<0:
					m = max([m, abs(self.A[i,k])])
			# print('m=%f'%m)
			assert(m>0)
			for j in range(self.n):
				if -self.A[i,j]>=self.eps * m: 
					S[i].add(j)
					# print(i,j)
			# exit()

		for i in range(self.n):
			St[i] = set([])
			for j in range(self.n):
				if i in S[j]: St[i].add(j)

		self.C = set([])
		self.F = set([])
		U = set(range(self.n))
		ll = [0] * self.n

		# print(St)

		while len(U)>0:
			self._compute_l(ll, St, U, self.F)
			i = ll.index(max(ll))
			# print(ll)
			# print(ll[i])
			assert(ll[i]>=0 and i in U)
			self.C.add(i)
			U.remove(i)
			for j in St[i]:
				if j in U: 
					self.F.add(j)
					U.remove(j)
		assert(len(self.C)+len(self.F)==self.n)
		assert(len(self.C & self.F)==0)

	def construct_P(self):
		nF = len(self.F)
		self.P = zeros((self.n, self.n))

		for i in self.F:
			aii = self.A[i,i]
			s = self.A[i,:].sum()
			s -= self.A[i,i]
			t = 0.0
			for j in self.C:
				t += self.A[i,j]
			# print(t)
			assert(t!=0)
			for j in self.C:
				self.P[i,j] = -s/t*self.A[i,j]/aii

		for i in self.C:
			self.P[i,i] = 1.0

		# print(self.P)
		self.P = self.P[:,sorted(list(self.C))] # important
		# print(self.P);

	def construct_Ac(self):
		self.Ac = dot(self.P.T, dot(self.A,self.P))

	def relax_fine(self):
		self.xk = solve(tril(self.A), self.b-dot(triu(self.A,1),self.xk))
		
		if self.sol is not None: 
			print('|Ax-b|=%g, |x-x^*|: %g'% \
				(norm(self.b-dot(self.A,self.xk)),norm(self.xk-self.sol)))
		# else:
		# 	print('Error: %g'%(norm(self.b-dot(self.A,self.xk))))

	def relax_coarse(self):
		r = self.b-dot(self.A,self.xk)
		rc = dot(self.P.T, r)
		Ec = solve(self.Ac, rc)
		E = dot(self.P, Ec)
		self.xk += E

	def directsolve(self):
		self.xk = solve(self.A, self.b)

	def residual(self):
		return norm(self.b-dot(self.A,self.xk))

class AMGTree:
	def __init__(self, A, b, xk = None,  sol=None, lastLayer=None):
		self.amgs = []
		if lastLayer is None: lastLayer = inf
		if xk is None: xk = ones(b.shape[0])
		C = A.shape[0]
		while len(self.amgs)<lastLayer:   
			assert(b.shape[0]==xk.shape[0]==A.shape[0])
			# print(A.shape, xk.shape)
			amg = AMG(A, b, xk = xk)
			self.amgs.append(amg)
			# print(amg.P)
			C = amg.Ac.shape[0]
			if C<=3: break
			A, b, xk = amg.Ac, dot(amg.P.T, amg.b - dot(amg.A, amg.xk)), dot(amg.P.T,amg.xk)
		print('There are %i layers'%len(self.amgs))

	def downstep(self):
		# print('(>)Residual at level 0:%g'%(self.amgs[0].residual()))
		for i in range(1,len(self.amgs)):
			amg = self.amgs[i]
			last_amg = self.amgs[i-1]
			last_amg.relax_fine()
			# print('(>>)Residual at level %i:%g'%(i-1, self.amgs[i-1].residual()))
			# print('(>)Residual at level %i:%g'%(i, self.amgs[i].residual()))
			amg.b = dot(last_amg.P.T, last_amg.b - dot(last_amg.A, last_amg.xk))
			# amg.xk = dot(last_amg.P.T, last_amg.xk) ## DO NOT PROPAGATE ERROR DOWNWARD
		# print('(>>)Residual at level %i:%g'%(i, self.amgs[i].residual()))

	def upstep(self):
		last_amg = self.amgs[-1]
		# print('(<)Residual at last level:%g'%(self.amgs[-1].residual()))
		last_amg.directsolve()
		# print('(<<)Residual at last level:%g'%(self.amgs[-1].residual()))
		

		for i in range(len(self.amgs)-2,-1,-1):
			# print('(<)Residual at level %i:%g'%(i, self.amgs[i].residual()))
			last_amg = self.amgs[i+1]
			amg = self.amgs[i]
			amg.xk += dot(amg.P,last_amg.xk)
			# print('(<<)Residual at level %i:%g'%(i, self.amgs[i].residual()))


	def iterate(self):
		self.downstep()
		self.upstep()
		# print('Residual at level 0:%g'%(self.amgs[0].residual()))
		# self.amgs[0].relax_fine()

		self.xk = self.amgs[0].xk




def genmat(N=3):
	B = array([[-4, 1, 0], [ 1, -4, 1], [ 0, 1, -4]],dtype=float64)
	Bs = [B]*N
	A = block_diag(*Bs)
	# Upper diagonal array offset by 3
	Dupper = diag(ones(3 * (N-1)), 3)
	# Lower diagonal array offset by -3
	Dlower = diag(ones(3 * (N-1)), -3)
	A += Dupper + Dlower
	return -A




A = genmat(400)
b = ones(A.shape[0])
sol = solve(A,b)

amg = AMGTree(A, b, xk=ones(b.shape[0]),lastLayer=4)
tol = 1
k = 0
while tol>1e-10:
	amg.iterate()
	print('Iteration#%i,error:%g'%(k, norm(b-dot(A,amg.xk))))
	tol = norm(b-dot(A,amg.xk))
	k+=1
	if k==20: break


exit(0)


# ## Iterative method test
# A = genmat(100)
# b = ones(A.shape[0])
# sol = solve(A,b)
# amg = AMG(A,b,sol=sol)
# res = 1
# k = 0
# err = []
# amg.relax_fine()
# while res>1e-12:
# 	amg.relax_fine()
# 	res = amg.residual()
# 	err.append(res)
# 	k += 1

# print('total iteration: %i'%k)
# plot(log(err))
# p = polyfit(range(len(err)), log(err),1)
# print(exp(p[0]))
# show()


# ## Two layer AMG test
# res = 1
# k = 0
# err = []
# A = genmat(100)
# b = ones(A.shape[0])
# sol = solve(A,b)
# amg = AMG(A,b,sol=sol)


# while res>1e-12:
# 	amg.relax_fine()
# 	amg.relax_coarse()
# 	res = amg.residual()
# 	err.append(res)
# 	k += 1
# print('total iteration: %i'%k)
# plot(log(err))
# p = polyfit(range(len(err)), log(err),1)
# print(exp(p[0]))
# show()

