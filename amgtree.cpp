#include "amgtree.h"

AMGTree::AMGTree(spMat *A, VectorXd *b,  VectorXd *sol, int lastLayer){
	A_ = A;
	b_ = b;
	xk_ = new VectorXd(b->size());
	*xk_ = Eigen::VectorXd::Ones(b->size());

	alist.push_back(new spMat(*A_));
	blist.push_back(new VectorXd(*b_));
	xlist.push_back(new VectorXd(*xk_));

	clock_t t_ = clock();

	int C = b->size();
	while(amgs.size()<lastLayer or lastLayer==-1){
		assert(alist.back()->cols()==blist.back()->size());
		assert(blist.back()->size()==xlist.back()->size());

		AMG* amg = new AMG(alist.back(), blist.back(), xlist.back());
		amgs.push_back(amg);
		C = amg->Ac_->rows();
		if(C<=3 or amgs.size()>lastLayer) break;
		alist.push_back(amg->Ac_);
		VectorXd * bb = new VectorXd(alist.back()->rows());
		// *bb = (amg->P_->transpose())*(*(amg->b_)-(*amg->A_)*(*amg->xk));
		blist.push_back(bb);
		xlist.push_back(new VectorXd(bb->size()));
		cout << "Layer "<<amgs.size()<< " has "<<amgs.back()->n_ << " Nodes."<<endl;
	}

	t1 = (clock()-t_)/(double)CLOCKS_PER_SEC;

	cout << "There are "<< amgs.size() << " layers."<<endl;

}

AMGTree::~AMGTree(){
	for(int i=0;i<amgs.size();i++){
		delete blist[i];
		delete xlist[i];
	}
	delete alist[0];
}

double AMGTree::downstep(){
	double t2;
	clock_t _t1 = clock();

	for(int i=1;i<amgs.size();i++){
		AMG* amg = amgs[i];
		AMG* last_amg = amgs[i-1];
		t2 = last_amg->relax_fine();
		*amg->b_ = (last_amg->P_->transpose())*((*last_amg->b_)-\
			(*last_amg->A_)*(*last_amg->xk));
	}

	printf("downstep total time %f\n", (clock()-_t1)/(double)CLOCKS_PER_SEC);
	return (clock()-_t1)/(double)CLOCKS_PER_SEC;
}

double AMGTree::upstep(){
	clock_t _t1 = clock();

	AMG * last_amg = amgs.back();
	last_amg->directsolve();

	for(int i=amgs.size()-2;i>-1;i--){
		last_amg = amgs[i+1];
		AMG * amg = amgs[i];
		(*amg->xk) += (*amg->P_)*(*last_amg->xk);
	}
	printf("upstep total time %f\n", (clock()-_t1)/(double)CLOCKS_PER_SEC);
	return (clock()-_t1)/(double)CLOCKS_PER_SEC;
}

double AMGTree::iterate(){
	clock_t _t1 = clock();
	downstep();
	upstep();
	*xk_ = *(amgs[0]->xk);
	printf("iteration total time %f\n", (clock()-_t1)/(double)CLOCKS_PER_SEC);
	return (clock()-_t1)/(double)CLOCKS_PER_SEC;
}


double AMGTree::residual(){
	return (*b_-(*A_)*(*xk_)).norm();
}




