#include "m_mat_amg.h"
#include "amgtree.h"
#include <Eigen/IterativeLinearSolvers>

int main(int argc, char *argv[]){
	int N;
	if(argc!=2 and argc!=3) {
		printf("Usage \n Two Layer: ./main <num_dim>\n Many Layers: ./main <num_dim> <num_Layer>\n");
		exit(1);
	}

	N = atoi(argv[1]);
	int lastLayer = 2;
	if(argc==3){
		lastLayer = atoi(argv[2]);
	}

	spMat A(3*N,3*N);
	A = generate_matrix(N);
	VectorXd b = Eigen::VectorXd::Ones(N*3);
	// VectorXd sol(N*3);
	// Eigen::ConjugateGradient<spMat> solver;
	// sol = solver.compute(A).solve(b);

	// cout << sol << endl;

	// AMG amg(&A,&b,&sol);

	// double res = 1.0;
	// int k=0;
	// while(res>1e-9){
	// 	amg.relax_fine();
	// 	amg.relax_coarse();
	// 	res = amg.residual();
	// 	printf("Iteration#%d, error=%g\n", k, res);
	// 	k += 1;
	// }


	if(lastLayer==0){
		VectorXd sol = VectorXd::Ones(b.size());
		double res = 1.0;
		clock_t t = clock();
		int k = 0;
		while(res>1e-9){
			sol = b - (A.triangularView<Eigen::StrictlyUpper>()*(sol));
			sol = A.triangularView<Eigen::Lower>().solve(sol);
			res = (b-A*sol).norm();
			cout << "#" << k <<  " Error = "<<res << endl;
			k += 1;
		}
		printf("Total time for Gauss-Seidel %f seconds\n",(clock()-t)/(double)CLOCKS_PER_SEC);
		return 1;

	}


	AMGTree amgtree(&A, &b, NULL, lastLayer);
	printf("Assemble successfully in %f seconds.\n", amgtree.t1);
	double res = 1.0;
	int k=0;
	clock_t t_ = clock();
	while(res>1e-9){
		amgtree.iterate();
		res = amgtree.residual();
		printf("Iteration#%-3d, |Ax^k-b|=%g\n", k, res);
		k += 1;
	}
	double t2 = (clock()-t_)/(double)CLOCKS_PER_SEC;
	printf("Solver information:\n=======================\n");
	printf("Total iterations:%d\n", k);
	printf("Assemble time:%f\n", amgtree.t1);
	printf("Iteration time:%f\n", t2);


	return 1;
}