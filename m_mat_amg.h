#ifndef m_mat_amg_h
#define m_mat_amg_h

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <set>
#include <iterator>
#include <cassert>
#include <iostream>
#include <stdio.h>
#include <Eigen/SparseLU>
#include <time.h>


using namespace std;

typedef Eigen::MatrixXd MatrixXd;
typedef Eigen::VectorXd VectorXd;
typedef Eigen::SparseMatrix<double,Eigen::RowMajor> spMat;
typedef std::vector< set<int> > SetVec_i;
typedef Eigen::Triplet<double> T;

class AMG{
private:
	std::vector<int> ll_;
	SetVec_i S, St;
	double _compute_l(vector<int>& ll, SetVec_i& St, set<int>& U, set<int>& F);
	double find_coarse_nodes();
	double construct_P();
	double construct_Ac();
	
public:
	bool verbose_ = true;
	double eps_ = 0.01;
	spMat * A_, *P_, *Ac_;
	int n_;
	VectorXd * b_, *sol_, *xk;
	set< int > C_, F_;
	map< int, int > Cmap;
	AMG(spMat * A, VectorXd * b, VectorXd * sol=NULL);
	double relax_fine();
	double relax_coarse();
	double residual();
	double directsolve();
	~AMG();
};
spMat generate_matrix(int N);

#endif