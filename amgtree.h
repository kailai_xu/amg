#ifndef amgtree_h
#define amgtree_h

#include "m_mat_amg.h"
#include <time.h>

typedef std::vector<AMG*> AMGLevel;
typedef std::vector<spMat*> AList;
typedef std::vector<VectorXd*> VList;

class AMGTree{
private:
public:
	AMGLevel amgs;
	AList alist;
	VList blist,xlist;
	spMat * A_;
	VectorXd * b_;
	VectorXd * xk_;
	double t1;
	AMGTree(spMat *A, VectorXd *b,  VectorXd *sol=NULL, int lastLayer=-1);
	~AMGTree();
	double downstep();
	double upstep();
	double iterate();	
	double residual();
};

#endif