#include "m_mat_amg.h"


AMG::AMG(spMat * A, VectorXd * b, VectorXd * sol){
	A_ = A;
	b_ = b;
	sol_ = sol;
	xk = new VectorXd(b->size());
	*xk = VectorXd::Ones(b->size());
	n_ = A_->rows();
	assert(A_->rows()==A->cols());

	double t1,t2,t3;
	t1 = find_coarse_nodes();
	t2 = construct_P();
	t3 = construct_Ac();
	printf("FindCoarseNodes %f sec, ConstructP %f sec, Construct_Ac %f sec\n",t1, t2, t3 );
}

AMG::~AMG(){
	delete P_;
	delete Ac_;
	delete xk;
}


double AMG::find_coarse_nodes(){
	clock_t t_ = clock();
	S.resize(n_); St.resize(n_);

	for(int i=0;i<A_->outerSize();i++){
		double m = 0.0;
		for(spMat::InnerIterator it(*A_, i);it; ++it){
			if(it.value()<0) m = max(-it.value(), m);
		}
		assert(m>0);
		for(spMat::InnerIterator it(*A_, i);it ; ++it){
			if(-it.value()>=eps_*m) 
				{
					S[i].insert(it.index());
				}
		}
	}

	for(int i=0;i<A_->outerSize();i++){
		for(spMat::InnerIterator it(*A_, i);it; ++it){
			if(S[it.index()].count(i)) St[i].insert(it.index());
		}
	}
	set<int> U;
	for(int i=0;i<n_;i++) U.insert(i);

	ll_.resize(n_);
	for(int i=0;i<n_;i++) ll_[i] = St[i].size();

	std::vector<int> ll(n_);
	while(U.size()>0){
		int i = distance(ll_.begin(),max_element(ll_.begin(),ll_.end()));
		assert(ll_[i]>=0);
		C_.insert(i);
		U.erase(i);
		ll_[i] = -1;
		for(int j: S[i]){
			if(ll_[j]>0) ll_[j] -= 1;
		}

		for(set<int>::iterator it=St[i].begin(); it!=St[i].end(); it++){
			if(U.count(*it)) 
				{
					F_.insert(*it);
					U.erase(*it);
					ll_[*it] = -1;
					for(int j: S[*it]){
						if(ll_[j]>0) ll_[j]+=2;
					}
				}
		}
	}
	double time = (clock()-t_)/(double)CLOCKS_PER_SEC;
	return time;
}

double AMG::construct_P(){
	clock_t t_ = clock(), t0_;
	double t1_ = 0.0;
	vector<T> triplets;
	// construct cmap
	int k = 0;
	for(set<int>::iterator itj=C_.begin(); itj!=C_.end(); itj++){
		Cmap[*itj] = k;
		k += 1;
	}

	for(set<int>::iterator it=F_.begin();it!=F_.end();it++ ){
		int i = *it;
		double aii = A_->coeff(i,i);
		double s = 0.0, t=0.0;
		int new_id = 0;
		for(spMat::InnerIterator it0(*A_,i); it0; ++it0) 
			{
				s+= it0.value();
				if(C_.count(it0.index())) {
					t+= it0.value();

					// if(St[it0.index()].count(*it))
					// 	triplets.push_back(T(*it, Cmap[it0.index()],-s/t*A_->coeff(*it, it0.index())/aii));

				}
			}
		s -= aii;
		assert(t!=0);
		
		t0_ = clock();
		for(set<int>::iterator itj=C_.begin(); itj!=C_.end(); itj++)
		{
			if(St[*itj].count(*it)) triplets.push_back(T(*it,new_id,-s/t*A_->coeff(*it,*itj)/aii));
			new_id+=1;
		} // slow

		t1_ += (clock()-t0_)/(double)CLOCKS_PER_SEC;


	}
	printf("P nodes: %f\n", t1_);

	int new_id = 0;
	for(set<int>::iterator itj=C_.begin(); itj!=C_.end(); itj++){
		triplets.push_back(T(*itj,new_id,1.0));
		new_id += 1;
	}

	P_ = new spMat(n_, C_.size());
	P_->setFromTriplets(triplets.begin(),triplets.end());
	return (clock()-t_)/(double)CLOCKS_PER_SEC;
}


double AMG::construct_Ac(){
	clock_t t_ = clock();
	Ac_ = new spMat(P_->cols(), P_->cols());
	*Ac_ = P_->transpose() * (*A_) * (*P_);
	return (clock()-t_)/(double)CLOCKS_PER_SEC;
}

double AMG::relax_fine(){
	clock_t t_ = clock();
	VectorXd vec = *b_ - (A_ -> triangularView<Eigen::StrictlyUpper>()*(*xk));
	*xk = A_ -> triangularView<Eigen::Lower>().solve(vec);
	return (clock()-t_)/(double)CLOCKS_PER_SEC;
}

double AMG::directsolve(){
	clock_t t_ = clock();
	Eigen::SparseLU<spMat> solver;
	solver.compute(*A_);
	*xk = solver.solve(*b_);
	printf("DirectSolve time%f\n", (clock()-t_)/(double)CLOCKS_PER_SEC);
	return (clock()-t_)/(double)CLOCKS_PER_SEC;
}

double AMG::relax_coarse(){
	clock_t t_ = clock();
	VectorXd r = *b_ - (*A_)*(*xk);
	VectorXd rc = P_->transpose()*r;
	Eigen::SparseLU<spMat> solver;
	solver.compute(*Ac_);
	VectorXd Ec = solver.solve(rc);
	VectorXd E = (*P_)*Ec;
	*xk += E;
	return (clock()-t_)/(double)CLOCKS_PER_SEC;
}

double AMG::residual(){
	if(sol_) return ((*sol_)-(*xk)).norm();
	return (*b_-(*A_)*(*xk)).norm();
}



spMat generate_matrix(int N){
	MatrixXd mat(N*3,N*3);
	MatrixXd B(3,3);
	B << -4,1,0,
		1,-4,1,
		0,1,-4;
	for(int i=0;i<N;i++)
		mat.block(i*3,i*3,3,3) = -B;
	for(int i=1;i<N;i++){
		mat.block(i*3, (i-1)*3,3,3) = -Eigen::Matrix3d::Identity();
		mat.block((i-1)*3, i*3, 3,3) = -Eigen::Matrix3d::Identity();
	}
	// cout << mat << endl;
	return mat.sparseView(1e-10);

}




