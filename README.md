# AMG

The code implements Section 4 in ![Algebraic Multi-grid Method](http://stanford.edu/~kailaix/files/Algebraci_Multigrid_Method.pdf). 

```
mkdir build
cd build 
cmake ..
make
./main 100 2
```